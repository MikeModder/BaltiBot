const { RichEmbed } = require('discord.js');

exports.run = async (client, msg) => {
    await msg.guild.fetchMembers();
    const search = msg.args.join(' ');
    let member = msg.member;
    if(msg.mentions.members.size > 0) member = msg.mentions.members.first();
    else if(search){
        const found = client.extras.findUser(client, msg.guild.id, search);
        if(found.size === 0) return msg.replyErr(msg.channel, `No users found matching ***${search}*** in this guild!`);
        if(found.size > 1) return msg.replyWarn(msg.channel, `There where multiple users found matching ***${search}***:\n${found.first(5).map(m => `***${m.user.tag}***`).join('\n')}`);
        if(found.size === 1) member = found.first();
    }
    const roles = member.roles
        .filter(r => r.id !== msg.guild.id)
        .sort((a, b) => b.position - a.position)
        .map(r => r.toString())
        .join(', ') || 'None';
    const embed = new RichEmbed();
    embed
        .setThumbnail(member.user.displayAvatarURL)
        .setColor(member.displayHexColor)
        .setTitle(`About ${member.user.tag}`)
        .addField('User ID', member.id)
        .addField('Tag', member.user.tag)
        .addField('Roles', roles)
        .addField('Nickname', member.nickname ? member.nickname : 'None');
    msg.channel.send(embed);
};

exports.help = { name: 'user', usage: '{prefix}user <@User/ID/Username>', desc: 'Get information about a user', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: true };