exports.run = (client, msg) => { // jshint ignore:line
    msg.replyErr(msg.channel, 'error');
};

exports.help = { name: 'dbg_error', usage: '{prefix}dbg_error', desc: 'Simulate an error to test replyErr', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: false, premium: false };