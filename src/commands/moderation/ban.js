exports.run = async (client, msg) => {
    const members = msg.mentions.members.array();
    if(!members || members.length === 0) return msg.replyErr(msg.channel, 'You must mention at least one user to ban!');
    const reason = msg.args.join(' ') || 'No reason given';
    for(var m of members){
        if(m.user.id === msg.author.id) return msg.replyErr(msg.channel, 'You can\'t ban yourself!');
        if(m.user.id === client.user.id) return msg.replyErr(msg.channel, 'You can\'t ban me! ~~well you can but you can\'t make me ban myself~~');
        if(m.user.id === msg.guild.owner.id) return msg.replyErr(msg.channel, 'You can\'t ban the server owner! That\'d just be stupid...');
        if(!msg.guild.members.get(m.user.id).bannable) return msg.replyWarn(msg.channel, `The user ***${m.user.tag}*** can't be banned!`);
        try {
            m.ban(reason);
            msg.replyOk(msg.channel, `***${m.user.tag}*** was banned successfully!`);
        } catch (e) {
            msg.replyErr(msg.channel, `There was an error banning ***${m.user.tag}***:\n${e}`);
        }
    }
};

exports.help = { name: 'ban', usage: '{prefix}ban [@User1] <@User2.. ect>', desc: 'Ban some bitches', aliases: ['bean'], USER_PERMS: ['BAN_MEMBERS'], BOT_PERMS: ['BAN_MEMBERS'], public: true, premium: false };