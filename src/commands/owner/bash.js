const exec = require('@freebroccolo/shelljs-promises').exec;

exports.run = async (client, msg) => {
    const script = msg.args.join(' ');
    try {
        let { stdout, stderr } = await exec(script, { silent: true });
        if(!stderr && !stdout) return msg.replyOk(msg.channel, 'Done with no output!');
        if(stdout) return msg.replyOk(msg.channel, `Done! stdout:\n\`\`\`${stdout}\`\`\``);
        if(stderr) return msg.replyErr(msg.channel, `Done! stderr:\n\`\`\`${stderr}\`\`\``);
    } catch (e) {
        msg.replyErr(msg.channel, `Error:\n\`\`\`${e}\`\`\``, 'Command error!');
    }
};

exports.help = { name: 'bash', usage: '{prefix}bash <command>', desc: 'Run console commands on the host.', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: false, premium: false };