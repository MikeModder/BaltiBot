exports.run = async (client, msg) => {
    const script = msg.args.join(' ');
    try {
        let result = await eval(script);  //eslint-disable-line
        if(typeof result !== 'string') result = require('util').inspect(result);
        msg.replyOk(msg.channel, result.length < 1999 ? `\`\`\`${result.replace(client.token, '[let\'s just say this is a token]]').replace('`', '\`')}\`\`\`` : 'Output over 2000 characters!');
    } catch (e) {
        msg.replyErr(msg.channel, `Error:\n\`\`\`${e}\`\`\``);
    }
};

exports.help = { name: 'eval', usage: '{prefix}eval <JavaScript>', desc: 'Evaluate some JS.', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: false, premium: false };