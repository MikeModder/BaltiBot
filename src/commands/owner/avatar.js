const isUrl = require('valid-url').isUri;

exports.run = (client, msg) => {
    const url = msg.args[0];
    if(!url) return msg.replyErr(msg.channel, 'You must provide a link to an image!');
    if(!isUrl(url)) return msg.replyErr(msg.channel, 'That doesn\'t look like a URL!');
    client.user.setAvatar(url)
        .then(() => {
            msg.replyOk(msg.channel, 'Avatar changed successfully!');
        })
        .catch(e => {
            msg.replyErr(msg.channel, `Oh no, something went wrong!\n\n${e}`);
        });
};

exports.help = { name: 'setavatar', usage: '{prefix}setavatar <url>', desc: 'Set the bots avatar.', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: false, premium: false };