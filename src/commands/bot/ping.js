exports.run = async (client, msg) => {
    const start = Date.now();
    const m = await msg.channel.send('Ping...');
    m.edit(`Pong! Local ping: \`${Math.floor(client.ping)}ms\` API: \`${Date.now() - start}ms\``);
};

exports.help = { name: 'ping', usage: '{prefix}ping', desc: 'What\'s the ping?.', aliases: ['pang', 'pung', 'peng'], USER_PERMS: [], BOT_PERMS: [], public: true, premium: false };