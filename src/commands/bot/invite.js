const { RichEmbed } = require('discord.js');

exports.run = async (client, msg) => {
    const embed = new RichEmbed()
        .setTitle('Aww, you want to invite me?')
        .setDescription('Wow, I\'m flattered! I\'m still a work in progress so things may break or I may go offline while bugs are being fixed or features are being added.')
        .addField('Invite', process.env.DISCORD_INVITE)
        .setColor('RANDOM')
        .setFooter('Made with 💙');
    msg.channel.send(embed);
};

exports.help = { name: 'invite', usage: '{prefix}invite', desc: 'Get an invite link for the bot', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: true, premium: true };